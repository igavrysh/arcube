//
//  AppDelegate.h
//  artkittest
//
//  Created by Ievgen Gavrysh on 7/30/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

