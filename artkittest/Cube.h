//
//  Cube.h
//  artkittest
//
//  Created by Ievgen Gavrysh on 8/21/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import <SceneKit/SceneKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Cube : SCNNode

+ (SCNMaterial *)currentMaterial;

- (instancetype)initAtPosition:(SCNVector3)position
                  withMaterial:(SCNMaterial *)material;

- (void)changeMaterial;

- (void)showLasers;

@end

NS_ASSUME_NONNULL_END
