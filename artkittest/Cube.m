//
//  Cube.m
//  artkittest
//
//  Created by Ievgen Gavrysh on 8/21/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import <SceneKit/SceneKit.h>

#import "Cube.h"
#import "GVRMaterial.h"
#import "CollisionCategory.h"

static int currentMaterialIndex = 0;

static int currentModelType = 1;

@interface Cube ()
@property (nonatomic, strong) SCNNode           *nodeModel;
@property (nonatomic, strong) NSMutableArray    *lasers;
@end

@implementation Cube

#pragma mark -
#pragma mark Class Methods

+ (SCNMaterial *)currentMaterial {
    NSString *materialName;
    
    switch (currentMaterialIndex) {
        case 0:
            materialName = @"rustediron-streaks";
            break;
    }
    
    return [[GVRMaterial materialNamed:materialName] copy];
}

#pragma mark -
#pragma mark Initializations and Deallocations

- (instancetype)initAtPosition:(SCNVector3)position
                  withMaterial:(SCNMaterial *)material
{
    self = [super init];
    
    SCNNode *nodeModel = nil;
    
    if (currentModelType == 0) {
        float dimension = 0.1;
        SCNBox *cube = [SCNBox boxWithWidth:dimension
                                     height:dimension
                                     length:dimension
                              chamferRadius:0];
        
        nodeModel = [SCNNode nodeWithGeometry:cube];
    } else {
        SCNScene *modelScene = [SCNScene sceneNamed:@"art.scnassets/model/venus.scn"];
        
        nodeModel = [modelScene.rootNode childNodeWithName:@"venus"
                                     recursively:NO];
        
        self.lasers = @[].mutableCopy;
        [@[@"laser_0", @"laser_1"] enumerateObjectsUsingBlock:^(NSString *name, NSUInteger idx, BOOL *stop) {
            SCNNode *laser = [modelScene.rootNode childNodeWithName:name
                                                        recursively:YES];
            
            /*laser.transform = [laser convertTransform:SCNMatrix4Identity
                                               toNode:self.nodeModel];
            */
            
            SCNMaterial *laserMaterial = [SCNMaterial new];
            laserMaterial.diffuse.contents = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.9];
            laserMaterial.locksAmbientWithDiffuse = YES;
            
            laserMaterial.transparency = 0.5;
            
            laser.geometry.materials = @[laserMaterial];
            
            [nodeModel addChildNode:laser];
            
            [laser setHidden:YES];
            
            [self.lasers addObject:laser];
        }];
    }

    nodeModel.geometry.materials = @[material];
    
    nodeModel.physicsBody = [SCNPhysicsBody bodyWithType:SCNPhysicsBodyTypeDynamic
                                                        shape:nil];
    
    nodeModel.scale = [self scaleVectorForNode:nodeModel targetHeight:1];
    
    nodeModel.physicsBody.mass = 2.0;
    nodeModel.categoryBitMask = CollisionCategoryCube;
    nodeModel.position = position;
    [self addChildNode:nodeModel];
    self.nodeModel = nodeModel;
    
    return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)changeMaterial {
    currentMaterialIndex = (currentMaterialIndex + 1) % 1;
    [self.childNodes firstObject].geometry.materials = @[[Cube currentMaterial]];
}

- (void)showLasers {
    [self.lasers enumerateObjectsUsingBlock:^(SCNNode *laser, NSUInteger idx, BOOL *stop) {
        [laser setHidden:NO];
    }];
}

#pragma mark -
#pragma mark Private Method

- (SCNVector3)scaleVectorForNode:(SCNNode *)node targetHeight:(float)height {
    SCNVector3 min, max;
    
    [node getBoundingBoxMin:&min max:&max];
    
    float scale = 2.03 * height / (max.z - min.z);
    
    return SCNVector3Make(scale, scale, scale);
}

@end
