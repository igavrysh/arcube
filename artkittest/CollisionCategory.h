//
//  CollisionCategory.h
//  artkittest
//
//  Created by Ievgen Gavrysh on 8/21/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#ifndef CollisionCategory_h
#define CollisionCategory_h

typedef NS_OPTIONS(NSUInteger, CollisionCategory) {
    CollisionCategoryBottom = 1 << 0,
    CollisionCategoryCube = 1 << 1
};

#endif /* CollisionCategory_h */
