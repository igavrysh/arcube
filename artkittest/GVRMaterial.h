//
//  GVRMaterial.h
//  artkittest
//
//  Created by Ievgen Gavrysh on 8/21/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GVRMaterial : NSObject

+ (SCNMaterial *)simpleFabricMaterial;

+ (SCNMaterial *)materialNamed:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
