//
//  ViewController.m
//  artkittest
//
//  Created by Ievgen Gavrysh on 7/30/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import "ViewController.h"
#import "CollisionCategory.h"
#import "Config.h"
#import "Cube.h"
#import "GVRMaterial.h"

@interface ViewController () <
    ARSCNViewDelegate,
    UIGestureRecognizerDelegate,
    SCNPhysicsContactDelegate
>

@property (nonatomic, strong) IBOutlet ARSCNView            *sceneView;
@property (nonatomic, strong) SCNNode                       *venusModel;
@property (nonatomic, strong) NSMutableArray                *boxes;
@property (nonatomic, strong) NSMutableDictionary           *planes;

@property (nonatomic, strong) Config                        *config;
@property (nonatomic, strong) ARWorldTrackingConfiguration  *arConfig;

@end
    
@implementation ViewController

#pragma mark -
#pragma mark Private Methods

- (void)setupScene {
    self.sceneView.delegate = self;
    
    self.planes = [NSMutableDictionary new];
    
    self.boxes = [NSMutableArray new];
    
    self.sceneView.antialiasingMode = SCNAntialiasingModeMultisampling4X;
    
    SCNScene *scene = [SCNScene new];
    self.sceneView.scene = scene;
}

- (void)setupPhysics {
    // Setup a plane few meters below the origin so when
    // other node falls on it just remove it
    SCNBox *bottomPlane = [SCNBox boxWithWidth:1000 height:0.5 length:1000 chamferRadius:0];
    
    SCNMaterial *bottomMaterial = [SCNMaterial new];
    
    bottomMaterial.diffuse.contents = [UIColor colorWithWhite:1.0 alpha:0.0];
    
    bottomPlane.materials = @[bottomMaterial];
    
    SCNNode *bottomNode = [SCNNode nodeWithGeometry:bottomPlane];
    
    bottomNode.position = SCNVector3Make(0, -10, 0);
    bottomNode.physicsBody = [SCNPhysicsBody bodyWithType:SCNPhysicsBodyTypeKinematic shape:nil];
    
    bottomNode.physicsBody.categoryBitMask = CollisionCategoryBottom;
    bottomNode.physicsBody.contactTestBitMask = CollisionCategoryCube;
    
    [self.sceneView.scene.rootNode addChildNode:bottomNode];
    self.sceneView.scene.physicsWorld.contactDelegate = self;
}

- (void)setupLights {
    self.sceneView.autoenablesDefaultLighting = NO;
    self.sceneView.automaticallyUpdatesLighting = NO;
    
    UIImage *envImage = [UIImage imageNamed:@"art.scnassets/environment/sphericalBlurred"];
    
    self.sceneView.scene.lightingEnvironment.contents = envImage;
    
    /*
    SCNProgram *program = [SCNProgram new];
    [program setVertexFunctionName:@"bloom_vertex"];

    [program setFragmentFunctionName:@"bloom_fragment"];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"technique"
                                                     ofType:@"plist"];
    NSDictionary *techniqueDict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    SCNTechnique *technique = [SCNTechnique techniqueWithDictionary: techniqueDict];
    
    self.sceneView.technique = technique;
     */
}

- (void)setupRecognizers {
    UITapGestureRecognizer *tapGestureRecognizer
    = [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(handleTapFrom:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.sceneView addGestureRecognizer:tapGestureRecognizer];
    
    UILongPressGestureRecognizer *explosionGestureRecognizer
    = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(handleHoldFrom:)];
    explosionGestureRecognizer.minimumPressDuration = 0.5;
    [self.sceneView addGestureRecognizer:explosionGestureRecognizer];
    
    
    UILongPressGestureRecognizer *hidePlanesGestureRecognizer
    = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(handleHidePlaneFrom:)];
    hidePlanesGestureRecognizer.minimumPressDuration = 1;
    hidePlanesGestureRecognizer.numberOfTapsRequired = 2;
    [self.sceneView addGestureRecognizer:hidePlanesGestureRecognizer];
}

- (void)explode:(ARHitTestResult *)hitResult {
    float explosionYOffset = 0.1;
    
    SCNVector3 position = SCNVector3Make(hitResult.worldTransform.columns[3].x,
                                         hitResult.worldTransform.columns[3].y - explosionYOffset,
                                         hitResult.worldTransform.columns[3].z
                                         );
    
    for (SCNNode *cubeNode in self.boxes) {
        SCNVector3 distance = SCNVector3Make(cubeNode.worldPosition.x - position.x,
                                             cubeNode.worldPosition.y - position.y,
                                             cubeNode.worldPosition.z - position.z
                                             );
        
        float len = sqrtf(powf(distance.x, 2.0)
                          + powf(distance.y, 2.0)
                          + powf(distance.z, 2.0)
                          );
        
        float maxDistance = 2;
        float scale = MAX(0, maxDistance - len);
        
        scale = powf(scale, 2) * 2;
        
        distance.x = distance.x / len * scale;
        distance.y = distance.y / len * scale;
        distance.z = distance.z / len * scale;
        
        [cubeNode.physicsBody applyForce:distance
                              atPosition:SCNVector3Make(0.05, 0.05, 0.05)
                                 impulse:YES
         ];
    }
}
- (void)insertSpotLight:(SCNVector3)position {
    SCNLight *spotLight = [SCNLight light];
    spotLight.type = SCNLightTypeSpot;
    spotLight.spotInnerAngle = 45;
    spotLight.spotOuterAngle = 45;
    
    SCNNode *spotNode = [SCNNode new];
    spotNode.light = spotLight;
    spotNode.position = position;
    
    spotNode.eulerAngles = SCNVector3Make(-M_PI / 2, 0, 0);
    [self.sceneView.scene.rootNode addChildNode:spotNode];
}

- (void)insertCube:(ARHitTestResult *)hitResult {
    SCNVector3 position = SCNVector3Make(
                                   hitResult.worldTransform.columns[3].x,
                                   hitResult.worldTransform.columns[3].y,
                                   hitResult.worldTransform.columns[3].z
                                   );
    
    //SCNMaterial *material = [GVRMaterial simpleFabricMaterial];
    SCNMaterial *material = [Cube currentMaterial];
    
    Cube *cube = [[Cube alloc] initAtPosition:position
                                 withMaterial:material];
    
    [self.boxes addObject:cube];
    
    [self.sceneView.scene.rootNode addChildNode:cube];
}

- (void)updateConfig {
    SCNDebugOptions options = SCNDebugOptionNone;
    
    Config *config = self.config;
    
    if (config.showWorldOrigin) {
        options |= ARSCNDebugOptionShowWorldOrigin;
    }
    
    if (config.showFeaturePoints) {
        options |= ARSCNDebugOptionShowFeaturePoints;
    }
    
    if (config.showPhysicsBodies) {
        options |= SCNDebugOptionShowPhysicsShapes;
    }
    
    self.sceneView.debugOptions = options;

    self.sceneView.showsStatistics = config.showStatistics;
}

- (void)logVector:(SCNVector3)vector message:(NSString *)message {
    NSLog(@"%@ (%f, %f, %f)",
          message,
          vector.x,
          vector.y,
          vector.z);
}

- (void)logBoundingBoxForNode:(SCNNode *)node message:(NSString *)message {
    SCNVector3 minVector, maxVector;
    [node getBoundingBoxMin:&minVector max:&maxVector];
    
    NSLog(@"%@ at\nminVector (%f, %f, %f) \nmaxVector(%f, %f, %f)",
          message,
          minVector.x,
          minVector.y,
          minVector.z,
          maxVector.x,
          maxVector.y,
          maxVector.z);
}

#pragma mark -
#pragma mark View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupScene];
    [self setupLights];
    [self setupPhysics];
    [self setupRecognizers];
    
    ARWorldTrackingConfiguration *configuration = [ARWorldTrackingConfiguration new];
    configuration.lightEstimationEnabled = YES;
    configuration.planeDetection = ARPlaneDetectionHorizontal;
    self.arConfig = configuration;
    
    Config *config = [Config new];
    config.showStatistics = NO;
    config.showWorldOrigin = NO;
    config.showFeaturePoints = YES;
    config.showPhysicsBodies = NO;
    config.detectPlanes = YES;
    
    self.config = config;
    [self updateConfig];
    
    // Stop the screen from dimming while we are using the app
    [UIApplication.sharedApplication setIdleTimerDisabled:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.sceneView.session runWithConfiguration:self.arConfig options:0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.sceneView.session pause];
}

#pragma mark -
#pragma mark Gesture Recognizers

- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
    CGPoint tapPoint = [recognizer locationInView:self.sceneView];
    
    NSArray<ARHitTestResult *> *result
        = [self.sceneView hitTest:tapPoint
                            types:ARHitTestResultTypeExistingPlaneUsingExtent];
    
    if (result.count == 0) {
        return;
    }
    
    ARHitTestResult *hitResult = result.firstObject;
    [self insertCube:hitResult];
}

- (void)handleHoldFrom:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    
    Cube *cube = (Cube *)[self.boxes lastObject];
    [cube showLasers];
    
    return;
    
    CGPoint holdPoint = [recognizer locationInView:self.sceneView];
    NSArray<ARHitTestResult *> *result
        = [self.sceneView hitTest:holdPoint
                            types:ARHitTestResultTypeExistingPlaneUsingExtent];
    if (result.count == 0) {
        return;
    }
    
    ARHitTestResult *hitResult = result.firstObject;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self explode:hitResult];
    });
}

- (void)handleHidePlaneFrom:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    
    for (NSUUID *planeId in self.planes) {
        [self.planes[planeId] hide];
    }
    
    ARWorldTrackingConfiguration *configuration
        = (ARWorldTrackingConfiguration *)self.sceneView.session.configuration;
    configuration.planeDetection = ARPlaneDetectionNone;
    [self.sceneView.session runWithConfiguration:configuration];
}

#pragma mark -
#pragma mark - SCNPhysicsContactDelegate

- (void)physicsWorld:(SCNPhysicsWorld *)world
       didBeginContact:(SCNPhysicsContact *)contact
{
    CollisionCategory contactMask = contact.nodeA.physicsBody.categoryBitMask
        | contact.nodeB.physicsBody.categoryBitMask;
    
    if (contactMask == (CollisionCategoryBottom | CollisionCategoryCube)) {
        if (contact.nodeA.physicsBody.categoryBitMask
            == CollisionCategoryBottom)
        {
            [contact.nodeB removeFromParentNode];
        } else {
            [contact.nodeA removeFromParentNode];
        }
    }
}

#pragma mark -
#pragma mark ARSCNViewDelegate

- (void)renderer:(id<SCNSceneRenderer>)renderer
    updateAtTime:(NSTimeInterval)time
{
    ARLightEstimate *estimate = self.sceneView.session.currentFrame.lightEstimate;
    
    if (!estimate) {
        return;
    }
    
    CGFloat intensity = estimate.ambientIntensity / 1000.0;
    self.sceneView.scene.lightingEnvironment.intensity = intensity;
}

- (void)renderer:(id<SCNSceneRenderer>)renderer
      didAddNode:(SCNNode *)node
       forAnchor:(ARAnchor *)anchor
{
    if (![anchor isKindOfClass:[ARPlaneAnchor class]]) {
        return;
    }
    
    Plane *plane = [[Plane alloc] initWithAnchor:(ARPlaneAnchor *)anchor
                                        isHidden:YES];
    
    [self.planes setObject:plane forKey:anchor.identifier];
    
    [node addChildNode:plane];
}

- (void)renderer:(id<SCNSceneRenderer>)renderer
   didUpdateNode:(SCNNode *)node
       forAnchor:(ARAnchor *)anchor
{
    Plane *plane = [self.planes objectForKey:anchor.identifier];
    if (plane == nil) {
        return;
    }
    
    [plane update:(ARPlaneAnchor *)anchor];
}

- (void)renderer:(id<SCNSceneRenderer>)renderer
   didRemoveNode:(SCNNode *)node
       forAnchor:(ARAnchor *)anchor
{
    [self.planes removeObjectForKey:anchor.identifier];
}

@end
