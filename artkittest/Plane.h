//
//  Plane.h
//  artkittest
//
//  Created by Ievgen Gavrysh on 7/31/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>

@interface Plane : SCNNode
@property (nonatomic, retain) ARPlaneAnchor *anchor;
@property (nonatomic, retain) SCNBox *planeGeometry;

- (instancetype)initWithAnchor:(ARPlaneAnchor *)anchor isHidden:(BOOL)hidden;

- (void)update:(ARPlaneAnchor *)anchor;

- (void)setTextureScale;

- (void)hide;

@end

