//
//  Plane.m
//  artkittest
//
//  Created by Ievgen Gavrysh on 7/31/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import "Plane.h"

@implementation Plane

- (instancetype)initWithAnchor:(ARPlaneAnchor *)anchor
                      isHidden:(BOOL)hidden
{
    self = [super init];
    
    self.anchor = anchor;
    float width = anchor.extent.x;
    float length = anchor.extent.z;
    
    float planeHeight = 0.01;
    
    self.planeGeometry = [SCNBox boxWithWidth:width
                                       height:planeHeight
                                       length:length
                                chamferRadius:0
                          ];
    
    // Instead of just visualizing the grid as a gray plane, we will render
    // it in some Tron style colours.
    SCNMaterial *material = [SCNMaterial new];
    
   
    
    UIImage *img = [UIImage imageNamed:@"art.scnassets/materials/tron/tron_grid"];
    material.diffuse.contents = img;
    
    SCNMaterial *transparentMaterial = [SCNMaterial new];
    transparentMaterial.diffuse.contents = [UIColor colorWithWhite:1.0
                                                             alpha:0.0];
    
    if (hidden) {
        self.planeGeometry.materials
        = @[transparentMaterial,
            transparentMaterial,
            transparentMaterial,
            transparentMaterial,
            transparentMaterial,
            transparentMaterial
            ];
    } else {
        self.planeGeometry.materials
        = @[material,
            material,
            material,
            material,
            material,
            material
            ];
    }
    
    SCNNode *planeNode = [SCNNode nodeWithGeometry:self.planeGeometry];
    
    planeNode.position = SCNVector3Make(0, -planeHeight / 2, 0);
    
    // Planes in SceneKit are vertical by default so we need to rotate 90degrees to match
    // planes in ARKit
    //planeNode.transform = SCNMatrix4MakeRotation(-M_PI / 2.0, 1.0, 0.0, 0.0);
    
    
    SCNPhysicsShape *physicsShape = [SCNPhysicsShape shapeWithGeometry:self.planeGeometry
                                                               options:nil];
    planeNode.physicsBody = [SCNPhysicsBody bodyWithType:SCNPhysicsBodyTypeKinematic
                                                   shape:physicsShape
                             ];
    
    [self setTextureScale];
    [self addChildNode:planeNode];
    
    return self;
}

- (void)update:(ARPlaneAnchor *)anchor {
    self.planeGeometry.width = anchor.extent.x;
    self.planeGeometry.length = anchor.extent.z;
    
    self.position = SCNVector3Make(anchor.center.x, 0, anchor.center.z);
    
    SCNNode *node = [self.childNodes firstObject];
    SCNPhysicsShape *physicsShape = [SCNPhysicsShape shapeWithGeometry:self.planeGeometry
                                                               options:nil];
    node.physicsBody = [SCNPhysicsBody bodyWithType:SCNPhysicsBodyTypeKinematic
                                              shape:physicsShape
                        ];
    
    [self setTextureScale];
}

- (void)setTextureScale {
    CGFloat width = self.planeGeometry.width;
    CGFloat height = self.planeGeometry.length;
    
    
    // As the width/height of the plane updates, we want our tron grid material to
    // cover the entire plane, repeating the texture over and over. Also if the
    // grid is less than 1 unit, we don't want to squash the texture to fit, so
    // scaling updates the texture co-ordinates to crop the texture in that case
    SCNMaterial *material = self.planeGeometry.materials[4];
    material.diffuse.contentsTransform = SCNMatrix4MakeScale(width, height, 1);
    material.diffuse.wrapS = SCNWrapModeRepeat;
    material.diffuse.wrapT = SCNWrapModeRepeat;
}

- (void)printAnchor:(ARPlaneAnchor *)anchor {
    NSLog(@"Plane with anchor coordinates .x: %f, .y: %f, .z: %f",
          anchor.extent.x,
          anchor.extent.y,
          anchor.extent.z
    );
}

- (void)hide {
    SCNMaterial *transparentMaterial = [SCNMaterial new];
    transparentMaterial.diffuse.contents = [UIColor colorWithWhite:1.0 alpha:0.0];
    self.planeGeometry.materials
        = @[transparentMaterial,
            transparentMaterial,
            transparentMaterial,
            transparentMaterial,
            transparentMaterial,
            transparentMaterial
            ];
}

@end
