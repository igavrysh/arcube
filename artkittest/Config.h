//
//  Config.h
//  artkittest
//
//  Created by Ievgen Gavrysh on 8/21/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Config : NSObject
@property BOOL showWorldOrigin;
@property BOOL showFeaturePoints;
@property BOOL showStatistics;
@property BOOL showPhysicsBodies;
@property BOOL detectPlanes;

@end

NS_ASSUME_NONNULL_END
