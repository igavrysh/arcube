//
//  main.m
//  artkittest
//
//  Created by Ievgen Gavrysh on 7/30/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
