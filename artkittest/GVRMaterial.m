//
//  GVRMaterial.m
//  artkittest
//
//  Created by Ievgen Gavrysh on 8/21/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

#import "GVRMaterial.h"

static NSMutableDictionary *materials;

@implementation GVRMaterial

+ (SCNMaterial *)simpleFabricMaterial {
    SCNMaterial *material = [SCNMaterial new];
    NSString *path = @"art.scnassets/materials/old-textured-fabric/old-textured-fabric-albedo";
    UIImage *image = [UIImage imageNamed:path];
    material.diffuse.contents = image;
    
    return material;
}

+ (SCNMaterial *)materialNamed:(NSString *)name {
    SCNMaterial *material = materials[name];
    
    if (material) {
        return material;
    }
    
    material = [SCNMaterial new];
    material.lightingModelName = SCNLightingModelPhysicallyBased;
    //material.lightingModelName = SCNLightingModelConstant;
    
    NSString *path = [NSString stringWithFormat:@"art.scnassets/materials/%@", name];
    
    NSString *diffuseName = [NSString stringWithFormat:@"%@/%@-albedo.png", path, name];
    material.diffuse.contents = [UIImage imageNamed:diffuseName];
    
    
    NSString *roughnessName = [NSString stringWithFormat:@"%@/%@-roughness.png", path, name];
    material.roughness.contents = [UIImage imageNamed:roughnessName];
    
    NSString *normalName = [NSString stringWithFormat:@"%@/%@-normal.png", path, name];
    material.normal.contents = [UIImage imageNamed:normalName];
    
    NSString *metalName = [NSString stringWithFormat:@"%@/%@-metal.png", path, name];
    material.metalness.contents = [UIImage imageNamed:metalName];
    

    NSArray *properties
        = @[material.diffuse,
            material.roughness,
            material.normal,
            material.metalness];
    
    [properties enumerateObjectsUsingBlock:^(SCNMaterialProperty *property, NSUInteger idx, BOOL *stop) {
        property.wrapT = SCNWrapModeRepeat;
        property.wrapS = SCNWrapModeRepeat;
    }];
     
    materials[name] = material;
    
    return material;
}

@end
